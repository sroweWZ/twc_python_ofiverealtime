
import logging
import datetime as dt
import pandas as pd
from o5rt import o5rt
import yaml

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def test_verifyForecast():
	
	# known MATLAB case
#	fc = pd.read_csv('tmp_test_getForecast_data.csv')
#	obs = pd.read_csv('tmp_test_getObservations_data.csv')

	bboxText = '147 -38,147 -28,155 -28,155 -38,147 -38'
	fc = o5rt.getForecast(cfg, 'ACCESSR_APS2', 'TEMPERATURE', 'H2M', dt.datetime(2018,6,1,0,0,0), dt.datetime(2018,6,1,0,0,0), dt.datetime(2018,6,1,18,0,0), bboxText)

	# get obs
	obs = o5rt.getObservations(cfg, dt.datetime(2018,6,1,12,0,0) - dt.timedelta(hours=3), dt.datetime(2018,6,1,12,0,0), bboxText)

	# test case
	(v, rmse, weightedBiases) = o5rt.verifyForecast(cfg, fc, obs)

#	assert(pd.util.testing.assert_frame_equal(fc.sort_index().sort_index(axis=1), truth.sort_index().sort_index(axis=1), check_dtype=False) == None)
