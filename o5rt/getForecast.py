
# --------------------------------------------------------------------------------------------------------------------------------------
#
# getForecast()
#
# This function:
#	- queries the O5 DB for all forecast data for given source_code, element_type_code, level_code, issued 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

def getForecast(cfg, source_code, element_type_code, level_code, issued, startTime, endTime, bbox):

#	% FIXME get dressed data where available. to be discarded when all dressed parameters are available in the DB FIXME
	orig_source_code = source_code;
	if element_type_code == 'TEMPERATURE' and level_code == 'H2M': # | (strcmp(element_type_code, 'PRESSURE') & strcmp(level_code, 'MSL'))):
		if source_code in {'ACCESSC_SY_APS2', 'ACCESSC_VT_APS2', 'ACCESSC_SA_APS2'}:
			source_code = 'ACCESSC_Dressed_V1'
		elif source_code == 'ACCESSR_APS2':
			source_code = 'ACCESSR_Dressed_V1'
		elif source_code == 'ECMWF_HRES':
			source_code = 'ECMWF_HRES_Dressed_V1'
		elif source_code == 'OPTICAST4_CONSENSUS':
			source_code = 'OPTICAST4_CONSENSUS'
		elif source_code == 'Consensus_V2':
			source_code = 'Consensus_V2'
		else:
			source_code = 'ACCESSR_Dressed_V1'

	else:
		if source_code == 'OPTICAST4_CONSENSUS':
			source_code = 'ACCESSR_APS2'
		else:
			source_code = 'ACCESSR_APS2';

#	% FIXME Dressed hack FIXME

	# construct sql query
	fmt = '%Y-%m-%d %H:%M:%S'
	sql = ('select st_x(gp.geographic_point) as \'long\', st_y(gp.geographic_point) as lat, gpa.numerical_value as z, gp.id, es.source_code, es.issued, e.timestamp, e.element_type_code, e.level_code, e.numerical_value as value '
			'from elements e '
			'inner join element_sets es on es.id = e.element_set_id '
			'inner join grid_point_mappings x on x.grid_point_id2 = e.grid_point_id and x.context_code = \'NEAREST_LS\' '
			'inner join grid_points gp on gp.id = x.grid_point_id1 '
			'inner join grid_point_attributes gpa on gpa.grid_point_id = gp.id and gpa.attribute_code = \'ELEVATION\' '
			'where gp.grid_code = \'OBSERVATIONS\' '
			'and es.source_code in (\'{}\') '
			'and es.issued = timestamp(\'{}\') '
			'and e.timestamp >= timestamp(\'{}\') '
			'and e.timestamp <= timestamp(\'{}\') '
			'and e.element_type_code = \'{}\' '
			'and e.level_code = \'{}\' '
			'and MBRContains(GeomFromText(\'POLYGON(({}))\'), gp.geographic_point) '
			'order by gp.id, es.source_code, e.timestamp, '
			'timediff(e.timestamp, es.issued), element_type_code, level_code; ').format(source_code, issued.strftime(fmt), startTime.strftime(fmt), 
																						endTime.strftime(fmt), element_type_code, level_code, bbox);

	# attempt the query
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		dateparse = lambda x: pd.to_datetime(float(x)-365*1971, unit='D')
#		fc = pd.read_csv('../tests/tmp_test_getForecast_data.csv', parse_dates=['timestamp'], date_parser=dateparse)
		fc = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# time interpolation to hourly intervals, id by id
	fc.set_index('timestamp', inplace=True)
	fc = fc.groupby('id').apply(lambda fctmp: fctmp.resample('H').interpolate('linear'))
	
	# forward-fill strings into the newly created gaps (strings aren't interpolated)
	fc.fillna(method='ffill', inplace=True)

#	% FIXME Dressed hack FIXME
	fc['source_code'] = orig_source_code;
#	% FIXME Dressed hack FIXME

	# convert index timestamp back to column
	fc.reset_index(['timestamp'], inplace=True)

	return fc

