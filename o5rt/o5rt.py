
# --------------------------------------------------------------------------------------------------------------------------------------
#
# o5rt
#
# Opticast5 realtime nudging module
#
# --------------------------------------------------------------------------------------------------------------------------------------


import datetime as dt
import logging
import os
import pandas as pd
import numpy as np
import mysql.connector

# sql date format
fmt = '%Y-%m-%d %H:%M:%S'


# --------------------------------------------------------------------------------------------------------------------------------------
#
# getLatestRuns()
#
# This function:
#	- queries the O5 DB for all runs between issuedStartTime and issuedEndTime, for given sourceCodes
#	- returns the most recent runs for each returned source_code
#
# --------------------------------------------------------------------------------------------------------------------------------------

def getLatestRuns(cfg, sourceCodes, issuedStartTime, issuedEndTime):

	sourceCodeStr = '\',\''.join(sourceCodes)
	logging.info('Getting latest runs from {} to {} amongst \'{}\''.format(issuedStartTime.strftime(fmt), issuedEndTime.strftime(fmt), sourceCodeStr))

	# construct sql query
	sql = ('select source_code, issued '
			'from element_sets '
			'where source_code in (\'{}\') '
			'and issued >= timestamp(\'{}\') '
			'and issued <= timestamp(\'{}\') '
			'order by source_code, issued desc; ').format(sourceCodeStr, issuedStartTime.strftime(fmt), issuedEndTime.strftime(fmt));

	# attempt the query
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		df = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.info(e)
 
	finally:
		db.close()


	# group by source_code and take the most recent issued date
	runs = df.groupby(by='source_code', as_index=False).first()

	return runs


# --------------------------------------------------------------------------------------------------------------------------------------
#
# getForecast()
#
# This function:
#	- queries the O5 DB for all forecast data for given source_code, element_type_code, level_code, issued 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

def getForecast(cfg, source_code, element_type_code, level_code, issued, startTime, endTime, bbox):

#	% FIXME get dressed data where available. to be discarded when all dressed parameters are available in the DB FIXME
	orig_source_code = source_code;
#	if element_type_code == 'TEMPERATURE' and level_code == 'H2M': # | (strcmp(element_type_code, 'PRESSURE') & strcmp(level_code, 'MSL'))):
#		if source_code in {'ACCESSC_SY_APS2', 'ACCESSC_VT_APS2', 'ACCESSC_SA_APS2'}:
#			source_code = 'ACCESSC_Dressed_V1'
#		elif source_code == 'ACCESSR_APS2':
#			source_code = 'ACCESSR_Dressed_V1'
#		elif source_code == 'ECMWF_HRES':
#			source_code = 'ECMWF_HRES_Dressed_V1'
#		elif source_code == 'OPTICAST4_CONSENSUS':
#			source_code = 'OPTICAST4_CONSENSUS'
#		elif source_code == 'Consensus_V2':
#			source_code = 'Consensus_V2'
#		else:
#			source_code = 'ACCESSR_Dressed_V1'

#	else:
#		if source_code == 'OPTICAST4_CONSENSUS':
#			source_code = 'ACCESSR_APS2'
#		else:
#			source_code = 'ACCESSR_APS2';

#	% FIXME end of dressed hack FIXME

	# note we ask for unders and overs of the forecast date limits so as to permit a cropping to the real forecast time limits after the query
	startTime_ = startTime - dt.timedelta(hours=6)
	endTime_ = endTime + dt.timedelta(hours=6)

	# construct sql query
	fmt = '%Y-%m-%d %H:%M:%S'
	sql = ('select st_x(gp.geographic_point) as \'long\', st_y(gp.geographic_point) as lat, gpa.numerical_value as z, gp.id, es.source_code, es.issued, e.timestamp, e.element_type_code, e.level_code, e.numerical_value as value '
			'from elements e '
			'inner join element_sets es on es.id = e.element_set_id '
			'inner join grid_point_mappings x on x.grid_point_id2 = e.grid_point_id and x.context_code = \'NEAREST_LS\' '
			'inner join grid_points gp on gp.id = x.grid_point_id1 '
			'inner join grid_point_attributes gpa on gpa.grid_point_id = gp.id and gpa.attribute_code = \'ELEVATION\' '
			'where gp.grid_code = \'OBSERVATIONS\' '
			'and es.source_code in (\'{}\') '
			'and es.issued = timestamp(\'{}\') '
			'and e.timestamp >= timestamp(\'{}\') '
			'and e.timestamp <= timestamp(\'{}\') '
			'and e.element_type_code = \'{}\' '
			'and e.level_code = \'{}\' '
			'and MBRContains(GeomFromText(\'POLYGON(({}))\'), gp.geographic_point) '
			'order by gp.id, es.source_code, e.timestamp, '
			'timediff(e.timestamp, es.issued), element_type_code, level_code; ').format(source_code, issued.strftime(fmt), startTime_.strftime(fmt), 
																						endTime_.strftime(fmt), element_type_code, level_code, bbox);

	# attempt the query
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		fc = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# time interpolation to hourly intervals, id by id, crop to startTime and endTime limits
	fc.set_index('timestamp', inplace=True)
	fc = fc.groupby('id').apply(lambda fctmp, st=startTime, et=endTime: fctmp.resample('H').interpolate('linear').fillna(method='ffill').truncate(st, et))
	
	# forward-fill strings into the newly created gaps (strings aren't interpolated)
	#fc.fillna(method='ffill', inplace=True)

#	% FIXME Dressed hack FIXME
	fc['source_code'] = orig_source_code;
#	% FIXME Dressed hack FIXME

	# convert index timestamp back to column
	fc.reset_index('timestamp', inplace=True)

	return fc


# --------------------------------------------------------------------------------------------------------------------------------------
#
# getForecasts()
#
# This function:
#	- loops through a list of runs and collects forecasts into a list
#	- reduces fc locations to the intersection of all collected forecasts' locations
#	- returns forecasts list
#
# --------------------------------------------------------------------------------------------------------------------------------------

def getForecasts(cfg, runs, fcStartTime, fcEndTime, bbox):

	fcs = [pd.DataFrame()]*5

	for i, run in runs.iterrows():

		for element_type, element_level in zip(cfg['output']['variables']['codes'], cfg['output']['variables']['levels']):

			fcs[i] = fcs[i].append(getForecast(cfg, run.source_code, 
													element_type, element_level, 
													run.issued, fcStartTime, fcEndTime, 
													bbox))
		# take intersection of all fc sites
		if i == 0:			
			intersectedIds = set(fcs[0].id)
		else:
			intersectedIds = intersectedIds & set(fcs[i].id)

	# reduce fcs to common sites only
	fcs = [fcs[i].loc[fcs[i].id.isin(intersectedIds),:] for i, _ in runs.iterrows()]

	return fcs


# --------------------------------------------------------------------------------------------------------------------------------------
#
# getObservations()
#
# This function:
#	- queries the O5 DB for t2m, dp2m, mslp and wind observations made 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

def getObservations(cfg, startTime, endTime, bbox):

	logging.info('Getting observations from {} to {}'.format(startTime.strftime(fmt), endTime.strftime(fmt)))

	# construct sql query
	sql = ("select st_x(gp.geographic_point) as 'long', st_y(gp.geographic_point) as lat, gpa.numerical_value as z, gp.id, e.timestamp, e.element_type_code, e.level_code, e.numerical_value as value "
			"from elements e "
			"inner join grid_points gp on gp.id = e.grid_point_id "
			"inner join grid_point_attributes gpa on gpa.grid_point_id = gp.id and gpa.attribute_code = 'ELEVATION' "
			"inner join element_sets es on es.id = e.element_set_id "
			"where gp.grid_code = 'OBSERVATIONS' "
			"and es.source_code = 'OBSERVATIONS' "
			"and e.timestamp >= timestamp('{}') "
			"and e.timestamp <= timestamp('{}') "
			"and e.element_type_code in ('TEMPERATURE', 'DEW_POINT', 'WIND_U', 'WIND_V', 'PRESSURE') "
			"and e.level_code in ('H2M', 'H10M', 'MSL') "
			"and MBRContains(GeomFromText('POLYGON(({}))'), gp.geographic_point) "
			"order by gp.id, e.timestamp, e.element_type_code, e.level_code; ").format(startTime.strftime(fmt), endTime.strftime(fmt), bbox);

	# attempt the query
	try:
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		obs = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# replace wind components with wind speed and wind direction
	ws = np.sqrt(obs[obs.element_type_code == 'WIND_U'].value**2 + obs[obs.element_type_code == 'WIND_V'].value**2)
	wd = np.arctan2(obs[obs.element_type_code == 'WIND_V'].value, obs[obs.element_type_code == 'WIND_U'].value) * 180/np.pi
	obs.loc[obs.element_type_code == 'WIND_U', 'value'] = ws
	obs.loc[obs.element_type_code == 'WIND_U', 'element_type_code'] = 'WIND_SPEED'
	obs.loc[obs.element_type_code == 'WIND_V', 'value'] = wd
	obs.loc[obs.element_type_code == 'WIND_V', 'element_type_code'] = 'WIND_DIRECTION'

	# add a timeOffset column, relative
	obs['timeOffset'] = obs.timestamp - endTime

	# remove all locations that don't contain temperature, dewpoint and wind observations (ie contain any NaN values)
	obs.dropna()

	return obs


# --------------------------------------------------------------------------------------------------------------------------------------
#
# verifyForecast()
#
# This function:
#	- colocates a forecast dataframe with observations dataframe
#	- interpolates model forecast values to observation times, allowing for bias derivations
#	- calculates an rmse table by id and element_type_code
#
# --------------------------------------------------------------------------------------------------------------------------------------

def verifyForecast(cfg, fc, obs):

	# rename the value column so as not to overwrite each other in a combined dataframe
	fc.rename(index=str, columns={"value": "fc_value"}, inplace=True)
	obs.rename(index=str, columns={"value": "obs_value"}, inplace=True)

	# incorporate the observations into the forecast dataframe
	# sort by id, element_code then timestamp
	fc = fc.set_index(['id', 'element_type_code', 'timestamp'])
	obs = obs.set_index(['id', 'element_type_code', 'timestamp'])

	# merge the two dataframes and restore indices to columns
	v = fc.combine_first(obs)
	v.reset_index(['id', 'element_type_code', 'timestamp'], inplace=True)
	obs.reset_index(['id', 'element_type_code', 'timestamp'], inplace=True)

	# interpolate forecasts to observations times, id by id, element_type_code by element_type_code
	v.fc_value = v.groupby(['id','element_type_code']).fc_value.apply(lambda vtmp: vtmp.interpolate('linear'))

	# forward fill observations to empty forecast times, id by id, element_type_code by element_type_code
	# so as to have a latest observation matching up with the latest available forecast time
	v.obs_value = v.groupby(['id','element_type_code']).obs_value.apply(lambda vtmp: vtmp.fillna(method='ffill'))

	# add biases column, setting missing biases as 0.0 to avoid downstream errors
	v['bias'] = (v.fc_value - v.obs_value).fillna(0)

	# calculate a rmse dataframe, grouped by id and element_type_code, used for ranking models
	rmse = v.groupby(['id','element_type_code']).apply(lambda vtmp: (vtmp.bias**2).mean()**0.5)

	# time weighting as a function of time distance, used for persistence nudging
	v['wt'] = np.exp(obs.timeOffset / np.timedelta64(1, 'h'))	# in hours

	# coming obs with v leads to NaN weights where there are sites with no obs
	# set these missing weights to 1.0 to avoid summing errors. applied, with bias = 0, this will not make any correction
	v['wt'].fillna(1.0, inplace=True)

	weightedBiases = v.groupby(['id','element_type_code']).apply(lambda vtmp: (vtmp.wt*vtmp.bias).sum()/vtmp.wt.sum()).reset_index(name='wbias')
	v.drop(columns=['wt'], inplace=True)

	# calculate the most recent biases
	latestBiases = v.groupby(['id','element_type_code']).last().reset_index()

	# return derived tables
	return (v, rmse, weightedBiases)


# --------------------------------------------------------------------------------------------------------------------------------------
#
# rankModels()
#
# This function:
#	- verifies a list of forecasts wrt the passed in observaitons
#	- calculates a score for each based on weighted parameter aggregates over ids
#	- returns the fc indexs sorted by score
#
# --------------------------------------------------------------------------------------------------------------------------------------

def rankModels(cfg, fcs, obs):

	# loop through the provided fcs
	scores = pd.DataFrame()

	for i, fc in enumerate(fcs):

		# verify wrt the provided obs
		(v, rmse, weightedBiases) = verifyForecast(cfg, fc, obs)

		# create a new column with same dimensions as wbias
		weightedBiases.loc[:,'wt'] = 0.0 * weightedBiases['wbias'].values

		# ascribe weights to the weight column
		for j, code in enumerate(cfg['technical']['evaluationVariables']['codes']):

			weight = cfg['technical']['evaluationVariables']['weights'][j]
			weightedBiases.wt.where(weightedBiases.element_type_code != code, weight, inplace=True)


		# skill score, absolute weighted biases for all variables summed over all locations
		scores.loc[i,'score'] = 1 / weightedBiases.groupby('id').apply(lambda vtmp: vtmp.wt * np.abs(vtmp.wbias)).sum()
		scores.loc[i,'source_code'] = fc['source_code'][0]
		scores.loc[i,'fcs_index'] = int(i)

	scores.sort_values('score', ascending=True, inplace=True)

	return scores


# --------------------------------------------------------------------------------------------------------------------------------------
#
# makeEnsembleForecast()
#
# This function:
#	- queries the O5 DB for all forecast data for given source_code, element_type_code, level_code, issued 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

def makeEnsembleForecast(cfg, fcs, scores):

	# sort forecasts by score
	fcs = [fcs[int(i)] for i in scores.fcs_index]

	# allocate for an ensemble forecast table
	ensfc = fcs[0].copy()
	ensfc.fc_value = 0.0
	ensfc.source_code = cfg['output']['source_code']

	# sum of top n scores
	totalScore = float(sum(scores.score[0:cfg['technical']['ensembleSize']]))

	# take the top n forecasts as they are ranked and construct ensemble of all forecast values
	for fi in range(cfg['technical']['ensembleSize']):
	
		# be sure that ids, timestamps, element_type_codes are aligned
		# if not, the ensemble will be rubbish, of course
		assert(all(ensfc.id == fcs[fi].id))
		assert(all(ensfc.timestamp == fcs[fi].timestamp))
		assert(all(ensfc.element_type_code == fcs[fi].element_type_code))

		ensfc.fc_value = ensfc.fc_value + (scores.score[fi] / totalScore) * fcs[fi].fc_value


	# TODO scrub any unrealistic values ? TODO
	# TODO update RHs TODO

	logging.info('Made {} member ensemble'.format(cfg['technical']['ensembleSize']))


	return ensfc


# --------------------------------------------------------------------------------------------------------------------------------------
#
# applyObsPersistence()
#
# This function:
#	- armed with time weighted biases, applies a persistence method to the given forecast
#
# --------------------------------------------------------------------------------------------------------------------------------------

def applyObsPersistence(cfg, fc, weightedBiases, currentTime):

	# time difference in hours between all fc leadtimes and the current time
	deltat = (fc.timestamp - currentTime)/dt.timedelta(hours=1)

	# time weighting as a function of leadtime (negative weights -> 0)
	fc['wt'] = ((np.log(cfg['technical']['persistenceHorizon']) - np.log(deltat + 1)) / np.log(cfg['technical']['persistenceHorizon'])).clip_lower(0)

	# add a weightedBiases column to the forecasts, effectively expanding the bias to all leadtimes
	fc = fc.merge(weightedBiases, on=['id', 'element_type_code'], how='right')

	# make sure sites without a weightedBias to correct with will not affected
	fc['wbias'].fillna(0, inplace=True)

	# ajusted forecast value by subtracting time weighted bias
	fc.fc_value = fc.fc_value + fc.wbias * fc.wt

	# remove working columns
	fc.drop(columns=['wt', 'wbias'], inplace=True)

	# remove any rows with any fc NaN values
	fc.dropna(subset=['fc_value'], inplace=True)

	return fc


# --------------------------------------------------------------------------------------------------------------------------------------
#
# publishForecast()
#
# This function:
#	- creates a new element_set for the latest realtime forecast
#	- add new elements to the elements table based on said forecast
#
# --------------------------------------------------------------------------------------------------------------------------------------

def publishForecast(cfg, fc, currentTime):

	# prepare an element_set dataframe describing this forecast
	fmt = '%Y-%m-%d %H:%M:%S'
	element_set = pd.DataFrame({'source_code': cfg['output']['source_code'],
								'issued': currentTime.strftime(fmt),
								'completed': dt.datetime.now().strftime(fmt),
								'create_system': cfg['output']['create_system'],
								'create_version': cfg['output']['create_version'],
								'create_source': os.path.basename(__file__),
								'update_system': cfg['output']['update_system'],
								'update_version': cfg['output']['update_version'],
								'update_source': os.path.basename(__file__)}, index=[0])

	# attempt to publish it to the element_sets table
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# send the element_set Pandas dataframe directly to the DB, updating any existing data
		dataframeToDB(element_set, db, 'element_sets')


		# query for the id of the element_set we just created so that the elements can point to it
		sql = "select id from element_sets where source_code = '{}' and issued = '{}'".format(cfg['output']['source_code'], currentTime.strftime(fmt))

		# query directly into a Pandas dataframe
		tmp = pd.read_sql(sql, con=db)
		element_set_id = tmp.id.values[0]

		# construct an elements dataframe to upload from our forecast dataframe
		elements = pd.DataFrame({'element_set_id': element_set_id,
								'grid_point_id': fc.id,
								'member': '1',
								'string_value': '',
								'create_system': cfg['output']['create_system'],
								'create_version': cfg['output']['create_version'],
								'create_source': os.path.basename(__file__),
								'update_system': cfg['output']['create_system'],
								'update_version': cfg['output']['create_version'],
								'update_source': os.path.basename(__file__),
								'element_type_code': fc.element_type_code,
								'level_code': fc.level_code,
								'numerical_value': fc.fc_value,
								'timestamp': fc.timestamp})

		# attempt the update of the elements table
		dataframeToDB(elements, db, 'elements')
	
		logging.info('Published {} elements to the {}'.format(len(elements), cfg['database']['host']))

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# return tables
	return (element_set, elements)


# --------------------------------------------------------------------------------------------------------------------------------------
#
# dataframeToDB()
#
# This function:
#	- inserts or updates a given dataframe into a given table of a given DB
#
# --------------------------------------------------------------------------------------------------------------------------------------

def dataframeToDB(df, db, table):

	columns = ', '.join([c for c in df.columns])
	values = ', '.join(['({})'.format(', '.join(["'{}'".format(v) for v in row[1].values])) for row in df.iterrows()])

	sql = "INSERT INTO {}({}) VALUES{} ON DUPLICATE KEY UPDATE last_update=VALUES(last_update)".format(table, columns, values)

	cursor = db.cursor()
	cursor.execute(sql)
	cursor.close()
	db.commit()


