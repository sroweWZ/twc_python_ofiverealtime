#!/usr/bin/python3

# --------------------------------------------------------------------------------------------------------------------------------------
#
# O5 nudging module
#
# This script:
#	- loads a configuration file containing a list of specific domains
#	- for each forecast domain:
#		- queries DB for a list of the most recent available model runs for said domain
#		- queries DB for the forecasts from these runs for said domain
#		- queries DB for observations during a previous recent history
#		- ranks the available model forecasts by verifying wrt to these recent observations
#		- creates a small ensemble forecast from the best of the available models
#		- applies local observation persistence to the ensemble forecast
#		- publish the realtime forecast to the DB
#		- outputs some diagnostic data if so desired
#
# --------------------------------------------------------------------------------------------------------------------------------------

import calendar
import datetime as dt
import logging
import os
import pandas as pd
import shutil
from subprocess import call
import mysql.connector
import sys
import time
import yaml

from o5rt import o5rt



# main function
if __name__ == "__main__":

	# load config file
	try:
		with open(sys.argv[1], 'r') as ymlfile:
			cfg = yaml.load(ymlfile)
	except:
		print('Could not load config file {}'.format(sys.argv[1]))
		raise

	# setup logging
	logging.basicConfig(filename=cfg['general']['logFile'], level=logging.getLevelName(cfg['general']['logLevel']), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	logging.debug('Config file {} read on startup'.format(sys.argv[1]))

	# assert that evaluation variables are a subset of forecast variables, otherwise the evaluation will fail
	assert(set(cfg['technical']['evaluationVariables']['codes']) < set(cfg['output']['variables']['codes']))

	# assert that output variables are unique (only one temperature forecast, most likely surface)
	assert(len(set(cfg['output']['variables']['codes'])) == len(cfg['output']['variables']['codes']))

	# set our reference datetimes
	currentTime = dt.datetime.strptime(sys.argv[2], cfg['general']['dateFormat'])
	runStartTime = currentTime - dt.timedelta(hours=cfg['technical']['runsLookbackStart'])
	runEndTime = currentTime - dt.timedelta(hours=cfg['technical']['runsLookbackEnd'])
	fcStartTime = currentTime
	fcEndTime = currentTime + dt.timedelta(hours=cfg['technical']['forecastDuration'])
	obsStartTime = currentTime - dt.timedelta(hours=cfg['technical']['obsLookback'])
	obsEndTime = currentTime

	logging.info('Current time: {}'.format(currentTime.strftime('%Y-%m-%d %H:%M:%S')))

	# loop over domains
	for dom in cfg['domains']:

		logging.info('Processing domain {}'.format(dom))

		# get a table of the latest available model runs amonst our candidates for this domain 
		runs = o5rt.getLatestRuns(cfg, cfg['domains'][dom]['models'], runStartTime, runEndTime)

		# collect all available forecasts within this bounding box for each output variable
		fcs = o5rt.getForecasts(cfg, runs, fcStartTime, fcEndTime, cfg['domains'][dom]['bbox'])

		# get obs within the lookback period
		obs = o5rt.getObservations(cfg, obsStartTime, obsEndTime, cfg['domains'][dom]['bbox'])

		# rank forecasts (will perform an internal verification)
		scores = o5rt.rankModels(cfg, fcs, obs)

		# create the small ensemble forecast based on the rankings
		ensfc = o5rt.makeEnsembleForecast(cfg, fcs, scores)

		# verify the ensemble forecast to obtain recent biases used for persistence correction
		(recentVerification, rmse, weightedBiases) = o5rt.verifyForecast(cfg, ensfc, obs)

		# apply observation persistence correction to obtain final forecast
		fc = o5rt.applyObsPersistence(cfg, ensfc, weightedBiases, currentTime)

		# TODO merge back to consesnsus after X hours - hopefully done downstream by blender? TODO

		# publish forecasts to the DB
		if cfg['output']['publish']:
			(element_set, elements) = o5rt.publishForecast(cfg, fc, currentTime)

		# write some results to file if so desired
		if cfg['diagnostic']['diagnosticMode']:
	
			for d in ['recentVerification', 'rmse', 'ensfc', 'fc', 'scores']:
				fname = '{}/{}_{}.csv'.format(cfg['diagnostic']['outputPath'], d, currentTime.strftime('%Y%m%d%H%M%S'))
				eval("{}.to_csv(fname, header=True, index=True, sep=',', mode='w')".format(d))


		# if in hindcasting mode verify wrt to "future" observations and save results
		if cfg['diagnostic']['hindcastMode']:

			dir_ = cfg['diagnostic']['outputPath']
			date_ = currentTime.strftime('%Y%m%d%H%M%S')

			obs = o5rt.getObservations(cfg, fcStartTime, fcEndTime, cfg['domains'][dom]['bbox'])
			(v, rmse, weightedBiases) = o5rt.verifyForecast(cfg, ensfc, obs)

			fname = '{}/hindcastVerification_{}.csv'.format(dir_, date_)
			v.to_csv(fname, header=True, index=True, sep=',', mode='w')



