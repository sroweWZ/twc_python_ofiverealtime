
# --------------------------------------------------------------------------------------------------------------------------------------
#
# verifyForecast()
#
# This function:
#	- colocates a forecast dataframe with observations dataframe
#	- interpolates model forecast values to observation times, allowing for bias derivations
#	- calculates an rmse table by id and element_type_code
#	- writes results to file if desired
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

def verifyForecast(cfg, fc, obs, write=False):

	# rename the value column so as not to overwrite each other in a combined dataframe
	fc.rename(index=str, columns={"value": "fc_value"}, inplace=True)
	obs.rename(index=str, columns={"value": "obs_value"}, inplace=True)

	# incorporate the observations into the forecast dataframe
	# sort by id, element_code then timestamp
	fc = fc.set_index(['id', 'element_type_code', 'timestamp'])
	obs = obs.set_index(['id', 'element_type_code', 'timestamp'])

	# merge the two dataframes and restore indices to columns
	v = fc.combine_first(obs)
	v.reset_index(['id', 'element_type_code', 'timestamp'], inplace=True)
	obs.reset_index(['id', 'element_type_code', 'timestamp'], inplace=True)

	# interpolate forecasts to observations times, id by id, element_type_code by element_type_code
	v.fc_value = v.groupby(['id','element_type_code']).fc_value.apply(lambda vtmp: vtmp.interpolate('linear'))

	# forward fill observations to empty forecast times, id by id, element_type_code by element_type_code
	# so as to have a latest observation matching up with the latest available forecast time
	v.obs_value = v.groupby(['id','element_type_code']).obs_value.apply(lambda vtmp: vtmp.fillna(method='ffill'))

	# add biases column and time-weighted biases
	v['bias'] = v.fc_value - v.obs_value

	# calculate a rmse dataframe, grouped by id and element_type_code, used for ranking models
	rmse = v.groupby(['id','element_type_code']).apply(lambda vtmp: (vtmp.bias**2).mean()**0.5)

	# calculate a timeWeightedBiases dataframe, used for persistence nudging
	v['wt'] = np.exp(obs.timeOffset*24)
	weightedBiases = v.groupby(['id','element_type_code']).apply(lambda vtmp: np.sum(vtmp.wt*vtmp.bias)/vtmp.wt.sum())

	# write results to file if so desired
	if write:
		fname = '/dev/null'.format()
		rmse.to_csv(fname, header=None, index=None, sep=',', mode='w')
	
	# return derived tables
	return (v, rmse, weightedBiases)

