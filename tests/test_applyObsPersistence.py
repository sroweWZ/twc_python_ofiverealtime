
import logging
import datetime as dt
import pandas as pd
import yaml
from o5rt import o5rt

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def test_applyObsPersistence():
	
	# get forecast
	startTime = dt.datetime(2018,6,2,0,0,0)
	bboxText = '147 -38,147 -28,155 -28,155 -38,147 -38'
	fc = o5rt.getForecast(cfg, 'ACCESSR_APS2', 'TEMPERATURE', 'H2M', startTime, startTime, startTime + dt.timedelta(days=1), bboxText)


	# get obs
	obs = o5rt.getObservations(cfg, startTime - dt.timedelta(hours=3), startTime, bboxText)

	# test case
	(v, rmse, weightedBiases) = o5rt.verifyForecast(cfg, fc, obs)

	# test case
	cfc = o5rt.applyObsPersistence(cfg, fc, weightedBiases, dt.datetime(2018,6,2,0,0,0))

	import pdb; pdb.set_trace()

