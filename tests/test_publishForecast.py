
import logging
import datetime as dt
import pandas as pd
from o5rt import o5rt
import yaml

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def test_publishForecast():
	
	# get forecast
	startTime = dt.datetime(2018,6,2,0,0,0)
	bboxText = '147 -38,147 -28,155 -28,155 -38,147 -38'
	fc = o5rt.getForecast(cfg, 'ACCESSR_APS2', 'TEMPERATURE', 'H2M', startTime, startTime, startTime + dt.timedelta(days=1), bboxText)

	# test case
	o5rt.publishForecast(cfg, fc, dt.datetime(2018,6,2,0,0,0))

	# TODO getForecast to get latest forecast
#	pfc = getForecast(cfg, cfg['output']['source_code'], 'TEMPERATURE', 'H2M', startTime, startTime, startTime + dt.timedelta(days=1), bboxText)
#	assert(all(pfc == fc))

