
# --------------------------------------------------------------------------------------------------------------------------------------
#
# applyObsPersistence()
#
# This function:
#	- armed with time weighted biases, corrects the ensemble forecast
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

def applyObsPersistence(cfg, ensfc, weightedBiases, currentTime):

	# be sure that ids, timestamps, element_type_codes are aligned
	# if this don't work, pandas.add with multi-indexing may be useful
#	assert(ensfc.id == weightedBiases.id)
#	assert(ensfc.timestamp == weightedBiases.timestamp)
#	assert(ensfc.element_type_code == weightedBiases.element_type_code)

	# may need to loop over ids
	# probably won't work
	# maybe be better off calculating in verifyForecast
	w0 = weightedBiases.groupby(['id','element_type_code']).bias.head(1)	# FIXME units, latestBias?
	wt = max(0, (log(12) - log((ensfc.timestamp - min(ensfc.timestamp)) - 1))./log(12) * weightedBiases.groupby(['id','element_type_code']).bias)	# FIXME units
	ensfc.value = ensfc.groupby(['id','element_type_code']).value.apply(lambda vtmp: vtmp - wt)

	return ensfc


