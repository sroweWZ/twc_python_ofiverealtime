
# --------------------------------------------------------------------------------------------------------------------------------------
#
# rankModels()
#
# This function:
#	- verifies a list of forecasts wrt the passed in observaitons
#	- calculates a score for each based on weighted parameter aggregates over ids
#	- returns the forecasts ranked by minimal score
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

def rankModels(cfg, fcs, obs):

	# loop through the provided fcs
	score = np.inf*len(fcs)
	for i, fc in enumerate(fcs):

		# verify wrt the provided obs
		(v, rmse, weightedBiases) = verifyForecast(cfg, fc, obs)

		# calculate the aggregate, weighted score
		score[i] = v.groupby('id').apply(lambda vtmp: cfg['technical']['t2mWeight']*vtmp[vtmp.element_type_code == 'TEMPERATURE'].mostRecentBias + \
														cfg['technical']['mslpWeight']*vtmp[vtmp.element_type_code == 'PRESSURE'].mostRecentBias).sum()

	# return forecasts sorted by score, and said score
	if len(score) > 0:
		return ([x for _, x in sorted(zip(score, fcs))], [x for x, _ in sorted(zip(score, fcs))])
	else:
		return None

