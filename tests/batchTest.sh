#!/bin/bash

currentDate=`date -u -Iminutes --date "2018-06-02T00:00:00"`
loopendDate=`date -u -Iminutes --date "2018-09-01T00:00:00"`

until [ "$currentDate" == "$loopendDate" ]
do
	echo "../ofiverealtime.py ../resources/ofiverealtime.dev.yml $(date -u --date "$currentDate" +%Y%m%d%H%M)"
	../ofiverealtime.py ../resources/ofiverealtime.dev.yml $(date -u --date "$currentDate" +%Y%m%d%H%M)

	currentDate=$(date -u -Iminutes --date "$currentDate +30 minute")
done


