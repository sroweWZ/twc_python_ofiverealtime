# --------------------------------------------------------------------------------------------------------------------------------------
#
# publishForecast()
#
# This function:
#	- creates a new element_set for the latest realtime forecast
#	- add new elements to the elements table based on said forecast
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import os
import pandas as pd
import numpy as np
import mysql.connector

def publishForecast(cfg, fc, currentTime):

	# prepare an element_set dataframe describing this forecast
	fmt = '%Y-%m-%d %H:%M:%S'
	element_set = pd.DataFrame({'source_code': cfg['output']['source_code'],
								'issued': currentTime.strftime(fmt),
								'completed': dt.datetime.now().strftime('%Y%m%d%H%M'),
								'create_system': cfg['output']['create_system'],
								'create_version': cfg['output']['create_version'],
								'create_source': os.path.basename(__file__),
								'update_system': cfg['output']['update_system'],
								'update_version': cfg['output']['update_version'],
								'update_source': os.path.basename(__file__)})

	# attempt to publish it to the element_sets table
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# send the element_set Pandas dataframe directly to the DB, updating any existing data (append)
		#element_set.to_sql('element_sets', con=db, if_exists='append')

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# query for the id of the element_set we just created so that the elements can point to it
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		sql = 'select id from element_sets where source_code = \'{}\' and issued = \'{}\''.format(element_set.source_code, element_set.issued)

		# query directly into a Pandas dataframe
		#tmp = pd.read_sql(sql, con=db)
		#element_set_id = tmp.element_set

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()


	# construct an elements dataframe to upload from our forecast dataframe
	elements = pd.DataFrame({'element_set_id': element_set_id,
							'grid_point_id': fc.id,
							'member': '1',
							'string_value': '',
							'create_system': cfg['output']['create_system'],
							'create_version': cfg['output']['create_version'],
							'create_source': os.path.basename(__file__),
							'update_system': cfg['output']['create_system'],
							'update_version': cfg['output']['create_version'],
							'update_source': os.path.basename(__file__),
							'element_type_code': fc.element_type_code,
							'level_code': fc.level_code,
							'numerical_value': fc.value,
							'timestamp': fc.timestamp})

	# attempt the update of the elements table
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# send the forecast Pandas dataframe directly to the DB, updating any existing data (append)
		#elements.to_sql('elements', con=db, if_exists='append')

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()


