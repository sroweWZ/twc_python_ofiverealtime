
import logging
import datetime as dt
import pandas as pd
from o5rt import o5rt
import yaml 

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)


logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')




def test_getLatestRuns():
	
	# known case
	truth = pd.DataFrame({'source_code': ['ACCESSC_SY_APS2', 'ACCESSR_APS2', 'ECMWF_HRES'],
				'issued': dt.datetime(2018,3,1,12,0,0)})
	
	# known data
	runs = o5rt.getLatestRuns(cfg, {'ACCESSC_SY_APS2', 'ACCESSR_APS2', 'ECMWF_HRES'}, dt.datetime(2018,3,1,0,0,0), dt.datetime(2018,3,1,12,0,0))

	assert(pd.util.testing.assert_frame_equal(runs.sort_index().sort_index(axis=1), truth.sort_index().sort_index(axis=1)) == None)
