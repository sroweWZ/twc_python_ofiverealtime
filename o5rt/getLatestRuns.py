
# --------------------------------------------------------------------------------------------------------------------------------------
#
# getLatestRuns()
#
# This function:
#	- queries the O5 DB for all runs between issuedStartTime and issuedEndTime, for given sourceCodes
#	- returns the most recent runs for each returned source_code
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import mysql.connector

fmt = '%Y-%m-%d %H:%M:%S'

def getLatestRuns(cfg, sourceCodes, issuedStartTime, issuedEndTime):

	sourceCodeStr = '\',\''.join(sourceCodes)
	logging.info('Getting latest runs from {} to {} amongst \'{}\''.format(issuedStartTime.strftime(fmt), issuedEndTime.strftime(fmt), sourceCodeStr))

	# construct sql query
	sql = ('select source_code, issued '
			'from element_sets '
			'where source_code in (\'{}\') '
			'and issued >= timestamp(\'{}\') '
			'and issued <= timestamp(\'{}\') '
			'order by source_code, issued desc; ').format(sourceCodeStr, issuedStartTime.strftime(fmt), issuedEndTime.strftime(fmt));

	# attempt the query
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		df = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.info(e)
 
	finally:
		db.close()


	# group by source_code and take the most recent issued date
	runs = df.groupby(by='source_code', as_index=False).first()

	return runs


