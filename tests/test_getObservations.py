
import logging
import datetime as dt
import pandas as pd
from o5rt import o5rt
import yaml

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def test_getObservations():
	
	# known MATLAB case
	# regions(1).bbox = [147 -38 147 -28 155 -28 155 -38 147 -38];
	# bboxText = sprintf('%d %d,', regions(1).bbox); regions(1).bboxText = bboxText(1:end-1); regions(1).name = 'NSW';
	# getObservations(Database.OFIVE, datenum(2018,03,01,12,00,00), datenum(2018,03,01,18,00,00), regions(1)); 
	# Grabbed ECMWF_HRES_Dressed_V1 H2M TEMPERATURE forecasts issued at 2018-03-01 12:00:00 from 2018-03-01 12:00:00 to 2018-03-01 18:00:00: 318 elementsgetForecast(Database.OFIVE, 'ECMWF_HRES', 'TEMPERATURE', 'H2M', datenum(2018,03,01,12,00,00), datenum(2018,03,01,12,00,00), datenum(2018,03,10,00,00,00), regions(1))
	truth = pd.read_csv('tmp_test_getObservations_data.csv')

	# test case
	bboxText = '147 -38,147 -28,155 -28,155 -38,147 -38'
	fc1 = o5rt.getObservations(cfg, dt.datetime(2018,6,1,12,0,0), dt.datetime(2018,6,1,18,0,0), bboxText)

	fc = fc1[1:10]

	assert(pd.util.testing.assert_frame_equal(fc.sort_index().sort_index(axis=1), truth.sort_index().sort_index(axis=1), check_dtype=False) == None)
