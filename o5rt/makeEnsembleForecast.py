
# --------------------------------------------------------------------------------------------------------------------------------------
#
# makeEnsembleForecast()
#
# This function:
#	- queries the O5 DB for all forecast data for given source_code, element_type_code, level_code, issued 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

def makeEnsembleForecast(cfg, fcsRanked, scores):

	# assuming all forecasts are for the same period and with the same frequency...
	ensfc = fc[0]
	ensfc.value = 0.0

	# sum of top n scores
	totalScore = float(sum(scores[0:cfg['technical']['ensembleSize']]))

	# take the top n forecasts as they are ranked and construct ensemble of all forecast values
	for fi in range(0, cfg['technical']['ensembleSize']):
	
		# be sure that ids, timestamps, element_type_codes are aligned
		# if this don't work, pandas.add with multi-indexing may be useful
		assert(ensfc.id == fc[fi].id)
		assert(ensfc.timestamp == fc[fi].timestamp)
		assert(ensfc.element_type_code == fc[fi].element_type_code)

		ensfc.value = ensfc.value + (totalScore - scores[fi+1]) / totalScore) * fcs[fi].value

	# TODO scrub any unrealistic values TODO

	return ensfc

