#!/usr/bin/python3

# --------------------------------------------------------------------------------------------------------------------------------------
#
# O5 nudging module
#
# This script:
#	- loads a configuration file containing a list of specific domains
#	- for each forecast domain:
#		- queries DB for a list of the most recent available model runs for said domain
#		- queries DB for the forecasts from these runs for said domain
#		- queries DB for observations during a previous recent history
#		- ranks the available model forecasts by verifying wrt to these recent observations
#		- creates a small ensemble forecast from the best of the available models
#		- applies local observation persistence to the ensemble forecast
#		- publish the realtime forecast to the DB
#
# --------------------------------------------------------------------------------------------------------------------------------------

import calendar
import datetime as dt
import logging
import os
import pandas as pd
import shutil
from subprocess import call
import mysql.connector
import sys
import time
import yaml

from getLatestRuns import getLatestRuns
from getObservations import getObservations
from getForecast import getForecast
from verifyForecast import verifyForecast
from makeEnsemble import makeEnsemble
from applyObsPersistence import applyObsPersistence
from publishForecast import publishForecast
from rankModels import rankModels

#----------------------------------------------------------------------------------------#

# main function
if __name__ == "__main__":

	# load config file
	try:
		with open(sys.argv[1], 'r') as ymlfile:
			cfg = yaml.load(ymlfile)
	except:
		print('Could not load config file {}'.format(sys.argv[1]))
		raise

	# setup logging
	logging.basicConfig(filename=cfg['general']['logFile'], level=logging.getLevelName(cfg['general']['logLevel']), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	logging.debug('Config file {} read on startup'.format(sys.argv[1]))

	# set our reference datetimes
	try:
		currentTime = dt.datetime.strptime(sys.argv[2], cfg['general']['dateFormat'])
	except:
		currentTime = dt.datetime.now()
		logging.warn('Unable to parse input date {} with format {}. Taking {} as current time'.format(sys.argv[2], cfg['general']['dateFormat'], currentTime.strftime('%Y%m%d%H%M')))
	
	startTime = currentTime
	endTime = currentTime + dt.timedelta(hours=cfg['technical']['forecastDuration'])

	# loop over domains
	for dom in cfg['domains']:

		logging.info('Processing domain {}'.format(dom))

		# get a table of the latest available model runs amonst our candidates for this domain 
		runs = getLatestRuns(cfg, cfg['domains'][dom]['models'], startTime, endTime)

		# collect all available forecasts
		# TODO we want to generalise to several element_type_codes, notably TEMPERATURE, DEWPOINT, MSLP and WINDSPEED
		fcs = [None]*len(runs)
		for i, fc in runs.iterrows():
			fcs[i] = getForecast(cfg, fc.source_code, 'TEMPERATURE', 'H2M', fc.issued, startTime, endTime, cfg['domains'][dom]['bbox'])

		# get obs within the lookback period
		obs = getObservations(cfg, startTime - dt.timedelta(hours=cfg['technical']['obsLookback']), startTime, cfg['domains'][dom]['bbox'])

		# rank forecasts (will perform an internal verification)
		(fcsRanked, scores) = rankModels(cfg, fcs, obs)

		# FIXME forecasts need to be temporally, spatially and element-wise identical for the ensemble to work correctly FIXME
		# they also need obsLookback data before the analysis time so that time weighted biases can be calculated

		# create the small ensemble forecast based on the rankings
		ensfc = makeEnsembleForecast(cfg, fcsRanked, scores)	# TODO

		# verify the ensemble forecast to obtain recent biases used for persistence correction
		(v, rmse, weightedBiases) = verifyForecast(cfg, ensfc, obs)

		# apply observation persistence correction to obtain final forecast
		fc = applyObsPersistence(cfg, ensfc, weightedBiases)

		# TODO merge back to consesnsus after X hours - hopefully done downstream by blender? TODO

		# publish forecasts to the DB
		publishForecasts(cfg, fc, currentTime)

		# if in hindcasting mode, ie the currentTime given is well in the past, verify wrt to "future" observations and save results
		if (dt.datetime.now() - currentTime).days > 1:
			obs = getObservations(cfg, startTime, endTime, cfg['domains'][dom]['bbox'])
			(v, rmse, weightedBiases) = verifyForecast(cfg, ensfc, obs, write=True)


