
import logging
import datetime as dt
import pandas as pd
import yaml

from o5rt import o5rt

with open('../resources/ofiverealtime.dev.yml', 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

logging.basicConfig(filename='test_errors.log', level=logging.getLevelName('INFO'), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def test_makeEnsemble():
	
	# known case
	startTime = dt.datetime(2018,6,2,0,0,0)
	runs = o5rt.getLatestRuns(cfg, {'ACCESSR_APS2', 'ECMWF_HRES', 'ACCESSC_SY_APS2'}, startTime - dt.timedelta(days=1), startTime)

	# get forecasts
	# keep track of common ids
	bboxText = '147 -38,147 -28,155 -28,155 -38,147 -38'
	fcs = [None]*len(runs)
	for i, fc in runs.iterrows():
		fcs[i] = o5rt.getForecast(cfg, fc.source_code, 'TEMPERATURE', 'H2M', fc.issued, startTime, startTime + dt.timedelta(days=1), bboxText)

		if i == 0:
			commonIds = set(fcs[0].id)
		else:
			commonIds = set(fcs[i].id) & commonIds

	# reduce fc ids to the common set
	for i, fc in runs.iterrows():
		fcs[i] = fcs[i].loc[fcs[i].id.isin(commonIds),:]

	# get obs
	obs = o5rt.getObservations(cfg, startTime - dt.timedelta(hours=3), startTime, bboxText)

	# rank forecasts
	(fcsRanked, scores) = o5rt.rankModels(cfg, fcs, obs)

	# make ensemble using scores
	fcsEns = o5rt.makeEnsembleForecast(cfg, fcsRanked, scores)

	# known ranking 0,1,2
#	assert(all([all(fcsRanked[i] == fcs[i]) for i in (0,1,2)]))
