
# --------------------------------------------------------------------------------------------------------------------------------------
#
# getObservations()
#
# This function:
#	- queries the O5 DB for t2m, dp2m, mslp and wind observations made 
#		between startTime and endTime and within a given bounding box bbox
#	- returns a pandas dataframe containing said data
#
# --------------------------------------------------------------------------------------------------------------------------------------

import datetime as dt
import logging
import pandas as pd
import numpy as np
import mysql.connector

fmt = '%Y-%m-%d %H:%M:%S'

def getObservations(cfg, startTime, endTime, bbox):

	logging.info('Getting observations from {} to {}'.format(startTime.strftime(fmt), endTime.strftime(fmt)))

	# construct sql query
	sql = ("select st_x(gp.geographic_point) as 'long', st_y(gp.geographic_point) as lat, gpa.numerical_value as z, gp.id, e.timestamp, e.element_type_code, e.level_code, e.numerical_value as value "
			"from elements e "
			"inner join grid_points gp on gp.id = e.grid_point_id "
			"inner join grid_point_attributes gpa on gpa.grid_point_id = gp.id and gpa.attribute_code = 'ELEVATION' "
			"inner join element_sets es on es.id = e.element_set_id "
			"where gp.grid_code = 'OBSERVATIONS' "
			"and es.source_code = 'OBSERVATIONS' "
			"and e.timestamp >= timestamp('{}') "
			"and e.timestamp <= timestamp('{}') "
			"and e.element_type_code in ('TEMPERATURE', 'DEW_POINT', 'WIND_U', 'WIND_V', 'PRESSURE') "
			"and e.level_code in ('H2M', 'H10M', 'MSL') "
			"and MBRContains(GeomFromText('POLYGON(({}))'), gp.geographic_point) "
			"order by gp.id, e.timestamp, e.element_type_code, e.level_code; ").format(startTime.strftime(fmt), endTime.strftime(fmt), bbox);

	# attempt the query
	try:
		
		db = mysql.connector.connect(host=cfg['database']['host'],
									user=cfg['database']['user'],
									passwd=cfg['database']['password'],
									database=cfg['database']['database'])

		# query directly into a Pandas dataframe
		obs = pd.read_sql(sql, con=db)

	except Exception as e:
		logging.warn(e)
 
	finally:
		db.close()

	# replace wind components with wind speed and wind direction
	ws = np.sqrt(obs[obs.element_type_code == 'WIND_U'].value**2 + obs[obs.element_type_code == 'WIND_V'].value**2)
	wd = np.arctan2(obs[obs.element_type_code == 'WIND_V'].value, obs[obs.element_type_code == 'WIND_U'].value) * 180/np.pi
	obs[obs.element_type_code == 'WIND_U'].value = ws
	obs[obs.element_type_code == 'WIND_U'].element_type_code = 'WIND_SPEED'
	obs[obs.element_type_code == 'WIND_V'].value = wd
	obs[obs.element_type_code == 'WIND_V'].element_type_code = 'WIND_DIRECTION'

	# add a timeOffset column, relative
	obs['timeOffset'] = obs.timestamp - endTime

	# remove all locations that don't contain temperature, dewpoint and wind observations (ie contain any NaN values)
	obs.dropna()

	return obs

